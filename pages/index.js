import Layout from "../components/Layout";
import Quote from "../components/quote";

export default function Index() {
  return (
    <Layout>
      <Quote />
    </Layout>
  );
}